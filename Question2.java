import java.util.Scanner;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * CSC 3002F Assignment 2 - Question 2
 * Sleeping Barber program
 * Program to coordinate the barber and the customers WITH DEALOCK.
 * 
 * @author Craig Feldman - FLDCRA001
 * 
 * Created		:	16 March 2014
 * Last Modified: 	18 March 2014
 *
 */
public class Question2 extends Thread {

		//semaphores - false != fair - Doesn't guarantee FIFO
	static PublicSemaphore customers = new PublicSemaphore(0, false); 		// Semaphore that controls # of cust in waiting room
	static PublicSemaphore barber =  new PublicSemaphore(0, false); 		// Binary Semaphore to indicate if barber is ready. barber initially asleep
	static PublicSemaphore accessSeat = new PublicSemaphore(1,false);		// Binary semaphore which will control access to var waiting - if 1, can access.
	
	public static int waiting = 0; 											//number of customers waiting for haircut
	public static int chairs; 												//total number of chairs in wr
	public static int numCustomers; 										//total number of customers that will come for a haircut

//==========================================================================================================
	public static void main(String[] args) {
		System.out.println("=========================================");
		System.out.println("\tWelcome to the Barber Shop");
		System.out.println("=========================================\n");
		
		//get input
		System.out.println("How many chairs are there in the waiting room?");
		System.out.print("> ");		
		Scanner keyboard = new Scanner(System.in);		
		chairs = keyboard.nextInt();		
		System.out.println("How many customers are going to arrive today?");
		System.out.print("> ");
		numCustomers = keyboard.nextInt();
		keyboard.close();
		
		System.out.println("There are " + chairs + " seats in the waiting room.\nA total of " + numCustomers + " customers will be arriving today.\n");
		
		Question2 simulation = new Question2();
		simulation.start();
	}
//==========================================================================================================
	
	public void run() {
		Barber bob = new Barber();
		bob.start(); //calls run
		
		//generate customers
		for (int i = 1; i <= numCustomers; ++i) {
			//simulate a pause for a length of time between customer arrivals
			try {
				//Min + (int)(Math.random() * ((Max - Min) + 1)) //random range , 1 to 5 seconds
				sleep(1000 + (int)(Math.random() * ((5000-1000) + 1)));
				
			} catch (InterruptedException e) {};
			
			Customer c = new Customer(i);
			c.start();
		}
	}
	

	/**
     * Subclass to expose protected methods - allows us to negate a release
     */
    static class PublicSemaphore extends Semaphore {
        PublicSemaphore(int permits) { super(permits); }
        PublicSemaphore(int permits, boolean fair) { super(permits, fair); }
        public void negateRelease() {
            super.reducePermits(1);
        }
    }

	
	

//==============================================================================================================
/** Manages activities of the barber */
class Barber extends Thread{
	
	public void run() {
		while (true) {				 //infinite loop so barber constantly tries to cut hair
			boolean slept = false; 
			try {
				if (waiting == 0) {
					System.out.println("SLEEP : There are no customers. Time to get some sleep.");
					slept = true;
				}
				customers.acquire(); // go to sleep if 0 customers
				if (slept)
					System.out.println("WAKE  : The barber has been woken.");

				//customer acquired, barber is awake, now we will check that we can -modify # of seats
				
				accessSeat.acquire();
				
				//seat # can be modified, customer acquired so we --waiting customers iff customer came from wr
				if (waiting != 0)
					--waiting;
				
				barber.release(); 		// signal that the barber is ready to cut
				accessSeat.release(); 	//release the lock on accessing the chairs because we have already performed ++
				
				// Barber is now ready to cut the acquired customers hair.
				cut();
			}
			
		 catch (InterruptedException e) 
		 {};
		} 	// while true
	} 		// run method
	

	//simulates cutting the customers hair.
	private void cut() {
		//System.out.println("START  : The barber is about to cut the hair of a new customer! Snip snip snip...");	
		//take some time to cut (2 to 5 secs)
		try {
			sleep(2000 + (int)(Math.random() * ((5000-2000) + 1)));
		} catch (InterruptedException e) {};
	}
}
//==========================================================================================================




//==========================================================================================================
/**
 * Manages activities of the customers
 */
class Customer extends Thread {
	
	int cust; 				// customer number
	boolean done = false;	//signals if a customer should leave (full or had haircut)
	
	//constructor that simply assigns a customer number
	public Customer(int i) 
	{cust = i;}
	
	public void run() {
		while (!done) {			// simulate multiple customers coming and going
			try {
				customers.release();
				if(barber.tryAcquire(0, TimeUnit.SECONDS)) { // attempts to acquire a barber but does not wait if it cannot do so. Fairness = true.
					 // barber was not busy							
					System.out.println("SKIP  : Customer #" + cust + " walked straight through the empty waiting room and woke the barber.");
					getCut();
					done = true; //had haircut so will leave
					break; 		 //finished so can break out of while
				}
				
				customers.negateRelease(); //did not acquire barber therefore must negate the release (will be called later if free chair)
	
//DEADLOCK					
				accessSeat.acquire();
				
				/*
				if (! accessSeat.tryAcquire(10, TimeUnit.SECONDS))
					System.out.println("ERROR: The system has detected a potential deadlock after not being able to acquire 'accessSeat' for a period of 10s.");
					*/

				//could not go straight to barber. will now check wr
				if (waiting < chairs) {
					++waiting; 				//take a seat
//THIS IS WHY A DEADLOCK OCCURS
					//accessSeat.release();
					customers.release(); 	// signal to barber there is a new customer waiting (adds 1 to semaphore)

					System.out.println("SEAT  : Customer #" + cust + " took a seat in the waiting room.");
						// have taken a seat, will now wait for barber or wake him
					try {
						barber.acquire(); 	//sleep if no barber
						getCut();
						done = true; 		//had haircut so will leave
					} catch (InterruptedException e) {
						e.printStackTrace(); }	
				}
				
				else { 			//no free chairs
					accessSeat.release();
					System.out.println("LEAVE : There are no free seats. Customer #" + cust + " left the barber.");
					done = true; //will leave
				}
				
			

			} catch (InterruptedException e) {
				e.printStackTrace(); }
					
		}	//while		
	}		//run

	//simulates customer getting a haircut
	private void getCut() {
		System.out.println("CUT   : Customer #" + cust + " is about to get a cut.");
	}

}
//==========================================================================================================


}	//Question1
